import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
import svgr from 'vite-plugin-svgr'
import tsconfigpaths from 'vite-tsconfig-paths'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), svgr(), tsconfigpaths()],
  resolve: {
    alias: {
      "@/assets/*": "./src/assets/*",
      "@/styles/*": "./src/styles/*",
      "@/components/*": "./src/components/*",
      "@/routes/*": "./src/routes/*"
    }
  }
})
