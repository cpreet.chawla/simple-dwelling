import {
    HeaderContainer,
    HeaderNavButton,
    HeaderLogoContainer,
    HeaderNav,
} from '@/styles/components/Header'
import Logo from '@/assets/Logo.svg?react'


const Header = () => {
    const dummyButtons = [
        "Home",
        "About",
        "Blogs",
        "Careers"
    ];

    return (
        <HeaderContainer>
            <HeaderLogoContainer>
                <Logo/>
            </HeaderLogoContainer>
            <HeaderNav>
            {
                dummyButtons.map((button) => 
                    <HeaderNavButton>{button}</HeaderNavButton>
                )
            }
            </HeaderNav>
        </HeaderContainer>
    )
}


export default Header
