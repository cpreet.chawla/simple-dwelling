import { StyledTextCarouselContainer, StyledTextCarousleSlide } from "@/styles/components/TextCarousel/ViewPort";
import { useRef, useState, useEffect } from "react";
import useInViewPort from "../../hooks/useInViewPort";

interface ViewPortProps {
  text: string[]
  delay?: number
  color?: string
}


const ViewPort = ({ 
  text, 
  delay = 1, 
  color = "black"
}: ViewPortProps) => {
  
  const [counter, setCounter] = useState(0);
  const sliderRef = useRef<HTMLSpanElement>(null);
  const isVisible = useInViewPort(sliderRef);

  useEffect(() => {
    const autoCounter = setInterval(() => {
      setCounter(prevCount => prevCount >= text.length - 1 ? 0 : prevCount + 1)
    }, delay * 1000)
    
    if(!isVisible){
      clearInterval(autoCounter);
    }
    return () => {
      clearInterval(autoCounter);
    }
  }, [isVisible, text.length, delay])

  useEffect(() => {
    if(isVisible) {
    sliderRef.current?.children?.item(counter)?.
    scrollIntoView({ 
        behavior: 'smooth', 
        block: 'nearest',
        inline: 'nearest'
    });

    }
  }, [isVisible, counter])
  
  return (
    <StyledTextCarouselContainer ref={sliderRef} >
      {
        text.map((item, index) => 
          <StyledTextCarousleSlide
            id={`textcarousel-${index}`}
            key={`textcarousel-${index}`}
            $inputColor={color}
          >
            {item}
          </StyledTextCarousleSlide>
        )
      }
    </StyledTextCarouselContainer>
  )
}

export default ViewPort;
