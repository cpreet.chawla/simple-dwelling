import ViewPort from "@/components/TextCarousel/ViewPort"

interface TextCarouselProps {
  text: string[],
  delay?: number,
  color?: string
}

const TextCarousel = (args: TextCarouselProps) => {
  return (
      <ViewPort {...args} />
  )
}

export default TextCarousel;
