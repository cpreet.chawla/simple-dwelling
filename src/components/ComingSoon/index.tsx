import { StyledComingSoonBylineContainer, StyledComingSoonContainer, StyledComingSoonSvgContainer } from '@/styles/components/ComingSoon';
import TextCarousel from '@/components/TextCarousel';

import ComingSoonSvg from '@/assets/comingsoon.svg?react';

const ComingSoon = () => {
  const dummyText=[
    "Switchs",
    "Bulbs",
    "Fans",
    "Air Conditioners",
    "Microwaves",
    "Refrigerators",
    "Life"
  ]
  return (
    <StyledComingSoonContainer>
      <StyledComingSoonSvgContainer>
        <ComingSoonSvg style={{ width: 'inherit', height: 'inherit' }} />
      </StyledComingSoonSvgContainer>
      <StyledComingSoonBylineContainer>
        <p style={{ margin: 0, marginRight: 5 }}>Automate your </p>
        <TextCarousel text={dummyText} color={"#0f81c7"}/>
      </StyledComingSoonBylineContainer>
    </StyledComingSoonContainer>
  )
}

export default ComingSoon
