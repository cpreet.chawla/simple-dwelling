import { HeroContainer } from "@/styles/components/Hero";
// import Carousel from "./Carousel";
// import AC from "@/assets/ACwiring.svg?react"
// import Bulb from "@/assets/Bulbwiring.svg?react"
// import Switch from "@/assets/Button1wiring.svg?react"
import TextCarousel from "./TextCarousel";
// import Fan from "@/assets/Fanwiring.svg?react"
// import Wire1 from "@/assets/Wire1wiring.svg?react"
// import Wire2 from "@/assets/Wire2wiring.svg?react"
// import Wire3 from "@/assets/Wire3wiring.svg?react"

const Hero = () => {
    const textList = [
      "Switches",
      "Microwaves",
      "Bulbs",
      "Fans",
      "Air Conditioners",
      "Everything"
    ]
    return (
        <HeroContainer>
          <h1> Let your house take care of you. </h1>
          <h2> Automate <TextCarousel text={textList}/> </h2>
       </HeroContainer> 
    )
}

export default Hero;
