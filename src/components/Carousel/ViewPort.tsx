import { 
  StyledViewPortSlide, 
  StyledViewPortSlider 
} from "@/styles/components/Carousel/ViewPort";
import { Children, useRef, useEffect } from 'react'; 

const ViewPort = ({ idx, children }: {idx: number} & React.PropsWithChildren) => {
  
  const viewPortRef = useRef<HTMLDivElement>(null);
  const mappedChildren = Children.map(children, (child, index) => 
    <StyledViewPortSlide
      id={`carousel-${index}`} 
      key={`carousel-${index}`}
    >{child}</StyledViewPortSlide>
  )

  useEffect(() => {
    if (viewPortRef.current !== null) {
      viewPortRef.current
      .children
      .item(idx)?.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start'})
    }
  }, [idx])

  return (
    <StyledViewPortSlider ref={viewPortRef}>
      {mappedChildren} 
    </StyledViewPortSlider>
  )
}

export default ViewPort; 
