import RightArrow from '@/assets/rightArrow.svg';

const RightButton = 
({ click }:{ click: () => void }) => {
  return (
    <button 
      onClick = {click}
      style={{ 
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        width: "64px", 
        height: "64px", 
        borderRadius: "4em"
      }}
    >
      <img src={RightArrow} width = "16px"/>
    </button>
  )
}

export default RightButton;
