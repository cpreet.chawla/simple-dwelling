import { PropsWithChildren, useState, useMemo, Children } from 'react';

import LeftButton from '@/components/Carousel/LeftButton';
import RightButton from '@/components/Carousel/RightButton';
import ViewPort from '@/components/Carousel/ViewPort';

const Carousel = ({ children }: PropsWithChildren) => {
  const [carouselIdx, setCarouselIdx] = useState(0);
  const slidesCount = useMemo(() => Children.count(children), [children]);
  
  const handleLeft = () => {
    carouselIdx <= 0 
    ?setCarouselIdx(slidesCount - 1)
    :setCarouselIdx(carouselIdx - 1)
  }

  const handleRight = () => {
    carouselIdx >= slidesCount - 1
    ?setCarouselIdx(0)
    :setCarouselIdx(carouselIdx + 1)
  }

  return (
    <div style={{ display: "flex", alignItems: 'center'}}>
      <LeftButton click={handleLeft}/>
      <ViewPort idx={carouselIdx}>
        {children}
      </ViewPort>
      <RightButton click={handleRight} />
    </div>
    
  )  
}

export default Carousel
