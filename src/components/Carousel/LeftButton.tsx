import LeftArrow from '@/assets/leftArrow.svg';

const LeftButton = (
{ click }: { click: () => void}
) => {
  return (
    <button
      onClick = {click}
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        width: "64px", 
        height: "64px", 
        borderRadius: "4em"
      }}
    >
      <img src={LeftArrow} width="16px"/>
    </button>
  )
}

export default LeftButton;
