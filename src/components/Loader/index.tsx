import { useMemo, useState, useEffect } from 'react';
import { StyledLoaderCircleContainer, StyledLoaderPercentageContainer, StyledLoaderCircle, StyledLoaderContainer } from "@/styles/components/Loader";
import LoaderLogo from "@/assets/simpleLogo.svg";

interface LoaderProps {
  width?: number,
  strokeWidth?: number,
  strokeLineCap?: 'round' | 'square' | 'butt',
  state?: 'preEnter' | 'entering' | 'entered' | 'preExit' | 'exiting' | 'exited' | 'unmounted'; 
}

const Loader = ({
  width = 200,
  strokeWidth = 5,
  strokeLineCap = 'round',
  state = 'entered'
}: LoaderProps) => {
  const [percentage, setPercentage] = useState(0);
  const PI = 3.14;
  const R = (width / 2) - (strokeWidth * 2);
  const circumference = 2 * PI * R;
  const gradientId = `#0f81c7#0f81c7`;

  const offset = useMemo(
    () => circumference - percentage / 100 * circumference,
    [percentage, circumference]
  )

  useEffect(() => {
    const completionController = setInterval(() => {
      setPercentage(percentage=> 
        { 
          if (percentage=== 100) {
            clearInterval(completionController)
            return 100
          }
          return percentage + 1 
        })
    }, 100)

    return () => {
      clearInterval(completionController);
    }
  }, [])
 
  return (
  <StyledLoaderContainer $state={state}>
    <StyledLoaderCircleContainer
      style={{
        width: `${width}px`,
        height: `${width}px`
      }}
    >
      <StyledLoaderPercentageContainer>
      <img src={LoaderLogo} width="50%" height="50%"/>
      </StyledLoaderPercentageContainer>
      <svg
        width="100%"
        height="100%"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
      >
      <linearGradient
        id={gradientId}
        x1="0%"
        y1="0%"
        x2="100%"
        y2="100%"
      >
        <stop offset='0%' stopColor={'#0f81c7'} />
        <stop offset='100%' stopColor={'#0f81c7'} />
      </linearGradient>
      <circle
          strokeWidth={strokeWidth}
          fill='transparent'
          r={R}
          cx={width / 2}
          cy={width / 2}
          stroke={'lightgray'}
          strokeDasharray={`${circumference} ${circumference}`}
      />
      <StyledLoaderCircle
          strokeWidth={strokeWidth}
          fill={'none'}
          r={R}
          cx={width / 2}
          cy={width / 2}
          stroke={`url(#${gradientId})`}
          strokeLinecap={strokeLineCap}
          strokeDasharray={`${circumference} ${circumference}`}
          strokeDashoffset={offset}
          transform="rotate(-90, 0, 0)"
      />
      </svg>
    </StyledLoaderCircleContainer>
    </StyledLoaderContainer>
  )
}

export default Loader; 
