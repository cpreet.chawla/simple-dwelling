import { useState, useEffect, useMemo, MutableRefObject } from 'react';

const useInViewPort = (ref: MutableRefObject<HTMLElement | null>) => {
  const [isIntersecting, setIsIntersecting] = useState(false);
  const observer = useMemo(() => 
    new IntersectionObserver(([entry]) => {
      setIsIntersecting(entry.isIntersecting)    
    }), 
  [])

  useEffect(() => {
    ref.current ? observer.observe(ref.current):() => {};

    return () => observer.disconnect();
  }, [ref, observer])

  return isIntersecting
}

export default useInViewPort;
