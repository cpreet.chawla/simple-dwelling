import { createBrowserRouter, RouterProvider } from "react-router-dom"
// import Root from "@/routes/Root"
// import Home from "@/routes/Home"
// import About from "@/routes/About"
import ComingSoon from "@/routes/ComingSoon"

// const router = createBrowserRouter([
//   {
//     path: "/",
//     element: <Root/>,
//     children: [
//       {
//         path: "/",
//         element: <Home />,
//       },
//       {
//         path: "/about",
//         element: <About />,
//       },
//     ]
//   }
// ])

const router = createBrowserRouter([
  {
    path: "/",
    element: <ComingSoon />
  }
])

const App = () => {
  return (
    <RouterProvider router={router} />
  )
}

export default App
