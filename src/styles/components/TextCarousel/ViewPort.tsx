import styled from 'styled-components';

export const StyledTextCarousleSlide = styled.p<{ $inputColor: string }>`
  display: inline-flex;
  width: auto;
  height: 100%;
  align-items: center;
  color: ${({ $inputColor }) => $inputColor };
`;

export const StyledTextCarouselContainer = styled.span`
  display: inline-flex;
  flex-direction: column;
  overflow-y: auto;
  height: 1em;
  
  -webkit-overflow-scrolling: touch;
  &::-webkit-scrollbar {
    display: none;
  }
`
