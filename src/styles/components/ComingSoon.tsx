import styled from "styled-components";

const sizes = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px',
  desktop: '2560px',
};

const devices = {
  mobileS: `(min-width: ${sizes.mobileS})`,
  mobileM: `(min-width: ${sizes.mobileM})`,
  mobileL: `(min-width: ${sizes.mobileL})`,
  tablet: `(min-width: ${sizes.tablet})`,
  laptop: `(min-width: ${sizes.laptop})`,
  laptopL: `(min-width: ${sizes.laptopL})`,
  desktop: `(min-width: ${sizes.desktop})`,
};

export const StyledComingSoonContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;

export const StyledComingSoonSvgContainer = styled.div`
  @media ${devices.mobileS} {
    width: 240px;
  }

  @media ${devices.mobileM} {
    width: 320px;
  }

  @media ${devices.mobileL} {
    width: 340px;
  }

  @media ${devices.laptopL} {
    width: 720px;
  }
`;

export const StyledComingSoonBylineContainer = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  
  @media ${devices.mobileS} {
    font-size: 1em;
  }

  @media ${devices.mobileM} {
    font-size: 1.2em;
  }

  @media ${devices.mobileL} {
    font-size: 1.5em;
  }

  @media ${devices.laptopL} {
    font-size: 2em;
  }
`;
