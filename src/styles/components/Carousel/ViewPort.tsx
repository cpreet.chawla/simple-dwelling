import styled from 'styled-components';

export const StyledViewPortSlider = styled.div`
  display: flex;
  overflow-x: auto;
  scroll-snap-type: x mandatory;
  scroll-behaviour: smooth;
  -webkit-overflow-scrolling: touch;

  width: 400px;

  &::-webkit-scrollbar{
    display: none;
  }
`

export const StyledViewPortSlide = styled.div`
  scroll-snap-align: center;
  flex-shrink: 0;
  width: 400px;
  height: 400px;
  border-radius: 10px;
  transform-origin: center center;
  transform: scale(1);
  transition: transform 0.5s;
  position: relative;
  
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 100px;
`;
