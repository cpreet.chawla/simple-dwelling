import styled from "styled-components";

export const HeaderContainer = styled.header`
    display: flex;
    width: 97.7%;
    max-height: 12rem;
    min-height: 9rem;
    margin: 20px;

    place-items: center;
    justify-content: space-evenly;
  
    background-color: #b1b2b5;
    border-radius: 20px;
`;

export const HeaderLogoContainer = styled.div`
`;

export const HeaderNav = styled.nav`
    
`;

export const HeaderNavButton = styled.button`
    /* Remove user agent styling */
    border-style: none;

    max-width: 130px;
    min-width: 100px;

    max-height: 50px;
    min-height: 45.5px;

    margin: 10px;
    border-radius: 20px;

    background-color: #848589;
`
