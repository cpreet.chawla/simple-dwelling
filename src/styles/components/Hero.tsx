import styled from "styled-components";

export const HeroContainer = styled.section`
    display: flex;
    flex-direction: column;
    height: 100vh;
    width: 100%;
    align-items: center;
    // border: 1px solid black;
    border-radius: 35px;
`;
