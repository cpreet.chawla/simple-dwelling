import styled from 'styled-components';

export const StyledLoaderContainer = styled.div<{ $state: string }>`
  position: fixed;
  width: 100vw;
  height: 100vh;
  
  display: flex;
  align-items: center;
  justify-content: center;
  background: #0f81c720;
  
  transition: all 1s;
  ${({ $state }) => (
    ($state === 'entered') &&
    `
      opacity: 0;
      transform: scale(1.5);
    `
  )}
`;

export const StyledLoaderPercentageContainer= styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const StyledLoaderCircleContainer = styled.div`
  display: inline-block;
  position: relative;
  bored-radius: 100%;
`;

export const StyledLoaderCircle = styled.circle`
  transform-origin: 50% 50%;
`;

