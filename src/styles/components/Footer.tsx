import styled from "styled-components";

export const FooterContainer = styled.footer`
    display: flex;
    
    background-color: black;

    width: 100%;

    max-height: 30vh;
    min-height: 20vh;

    border-radius: 40px;
`

export const FooterColumn = styled.div`

`;