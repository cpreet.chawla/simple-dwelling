import { useEffect, lazy } from 'react';
import { Outlet } from "react-router-dom";

import { useTransition as useTransitionAnimation } from "react-transition-state";

const Footer = lazy(() => import("@/components/Footer"));
const Header = lazy(() => import("@/components/Header"));
import Loader from '@/components/Loader';

import { StyledRootContainer } from "@/styles/routes/Root";

const Root = () => {
  const [{ status: state, isResolved }, toggle] = useTransitionAnimation({ 
      timeout: 5000,
      initialEntered: true,
      enter: true,
    })

  useEffect(() => {
    toggle()
  }, [toggle])

  return (
    <StyledRootContainer>
      <Loader state={state} />
      { isResolved && (
        <>
          <Header />
          <Outlet />
          <Footer />
        </>
      )}
    </StyledRootContainer>
  )
}
export default Root;
